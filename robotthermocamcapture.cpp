#include "robotthermocamcapture.h"

#include <Core/Containers/skarraycast.h>

ConstructorImpl(RobotThermocamCapture, SkFlowSat)
{
    enabled = false;
    publisher = nullptr;

    frameRawSize = 0;
    refreshRate = 0;    emissivity = 0.95f;
    gradient = 0;

#if defined(MLX98640_DEV)
    camSize = Size(32,24);

#elif defined(MLX98641_DEV)
    camSize = Size(16,12);

#endif

    matrixChan = -1;
    gradient = COLORMAP_TURBO;
    fps = 0;
    refreshValue = TCAP_FPS_NULL;

    compression = 0;

    setObjectName("RobotThermocamCapture");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotThermocamCapture::onSetup()
{
    SkCli *cli = skApp->appCli();

    i2cDev = cli->value("--i2c-device").toString();
    fps =  cli->value("--fps").toInt();

    AssertKiller((fps < 1) || (fps > 64));

    if (fps == 1)
        refreshValue = TCAP_FPS_1;

    else if (fps == 2)
        refreshValue = TCAP_FPS_2;

    else if (fps == 4)
        refreshValue = TCAP_FPS_4;

    else if (fps == 8)
        refreshValue = TCAP_FPS_8;

    else if (fps == 16)
        refreshValue = TCAP_FPS_16;

    else if (fps == 32)
        refreshValue = TCAP_FPS_32;

    else if (fps == 64)
        refreshValue = TCAP_FPS_64;

    gradient = COLORMAP_TURBO;

    if (!parseSize(objectName(), cli->value("--resolution").data(), &resolution))
        return false;

    frameRawSize = static_cast<ULong>(resolution.width * resolution.height * 3);
    heatImage = Mat(camSize, CV_32FC1);

    compression = cli->value("--compression").toInt();
    //

    publisher = new SkFlowVideoPublisher(this);
    publisher->setFlip(SK_HORIZONTAL_FLIPPING);
    publisher->setObjectName(this, "Publisher");

    setupPublisher(publisher);

    return true;
}

void RobotThermocamCapture::onInit()
{
#if defined(MLX98640_DEV)
    AssertKiller(!MLX9064x_I2CInit(i2cDev.c_str()))

    ObjectMessage("MLX98640 camera ENABLED on i2c bus");

    MLX90640_SetRefreshRate(MLX_I2C_ADDR, static_cast<uint8_t>(refreshValue));
    MLX90640_SetResolution(MLX_I2C_ADDR, 12);
    //MLX90640_SetChessMode(MLX_I2C_ADDR);

    MLX90640_DumpEE(MLX_I2C_ADDR, eeMLX90640);
    MLX90640_ExtractParameters(eeMLX90640, &mlx90640);
    refreshRate = MLX90640_GetRefreshRate(MLX_I2C_ADDR);

#elif defined(MLX98641_DEV)
    AssertKiller(!MLX9064x_I2CInit(i2cDev.c_str()));

    ObjectMessage("MLX98641 camera ENABLED on i2c bus");

    MLX90641_SetRefreshRate(MLX_I2C_ADDR, static_cast<uint8_t>(refreshValue));
    MLX90641_SetResolution(MLX_I2C_ADDR, 12);

    MLX90641_DumpEE(MLX_I2C_ADDR, eeMLX90641);
    MLX90641_ExtractParameters(eeMLX90641, &mlx90641);
    refreshRate = MLX90641_GetRefreshRate(MLX_I2C_ADDR);

#endif

    AssertKiller(refreshRate <= 0);
    ObjectMessage("MLX Enabled [refreshRate: " << refreshRate << "]");
    //

    publisher->setup(&f, resolution, fps, compression);
    publisher->start();

    ObjectMessage("Capture STARTED"
#if defined(MLX98640_DEV)
                  << " - device: MLX98640"
#elif defined(MLX98641_DEV)
                  << " - device: MLX98641"
#endif
                  << " - res: " << resolution
                  << " - fps: " << fps
                  << " - frameRawSize: " << publisher->getRawFrameSize() << " B");
}

void RobotThermocamCapture::onQuit()
{
    MLX9064x_I2Close();

    //

    publisher->stop();
    ObjectMessage("Capture STOPPED");

    f.clear();
}

void RobotThermocamCapture::onFastTick()
{
    if (enabled)
    {
        if (!publisher->hasTargets())
        {
            ObjectMessage("Capture tick NOT-ACTIVE");
            enabled = false;
            eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);
            return;
        }
    }

    else
    {
        if (publisher->hasTargets())
        {
            ObjectMessage("Capture tick ACTIVE");
            enabled = true;
            skApp->changeTimerMode(SK_FREELOOP);
        }

        else
            return;
    }

    grabData();
    buildPrimitiveFrame();

    try
    {
        // Normalize the mat
        normalize(heatImage, normal_mat, 0., 1.0, NORM_MINMAX, CV_32FC1);

        // Convert Mat to CV_U8 to use applyColorMap
        double minVal, maxVal;
        minMaxLoc(normal_mat, &minVal, &maxVal);

        normal_mat.convertTo(u8_mat, CV_8U, 255.0/(maxVal - minVal), -minVal);

        if (!SkImageUtils::doResize(u8_mat, size_mat, resolution))
            return;

        applyColorMap(size_mat, f.get(), gradient);
    }

    catch(Exception &e)
    {
        ObjectError("Producing output frame - " <<  e.msg);
    }

    f.resize(resolution);
    publisher->tick();
}

void RobotThermocamCapture::grabData()
{
#if defined(MLX98640_DEV)
    for (uint8_t x=0 ; x<2 ; x++)
    { //Read both subpages

        MLX90640_StartMeasurement(MLX_I2C_ADDR, x);

        int status = MLX90640_GetFrameData(MLX_I2C_ADDR, MLX90640_frame);

        if (status < 0)
            ObjectError("Cannot get frame [status: " << status << "]");

                //float vdd = MLX90640_GetVdd(mlx90640Frame, &mlx90640);
                float Ta = MLX90640_GetTa(MLX90640_frame, &mlx90640);

        float tr = Ta - TA_SHIFT; //Reflected temperature based on the sensor ambient temperature

        MLX90640_CalculateTo(MLX90640_frame, &mlx90640, emissivity, tr, termoMatrix);
    }

#elif defined(MLX98641_DEV)
    for (uint8_t x=0 ; x<2 ; x++)
    {
        int status = MLX90641_GetFrameData(MLX_I2C_ADDR, MLX90641_frame);

        if (status < 0)
            ObjectError("Cannot get frame [status: " << status << "]");

                //float vdd = MLX90641_GetVdd(MLX90641_frame, &mlx90641);
                float Ta = MLX90641_GetTa(MLX90641_frame, &mlx90641);

        float tr = Ta - TA_SHIFT; //

        MLX90641_CalculateTo(MLX90641_frame, &mlx90641, emissivity, tr, termoMatrix);
    }

#endif
}

void RobotThermocamCapture::buildPrimitiveFrame()
{
    try
    {

//MUST SUBSTITUTE WITH GetIMage (changing float *result to Mat &heatImage in library)
#if defined(MLX98640_DEV)
        for(int y=0; y<24; y++)
            for(int x=0; x<32; x++)
            {
                float val;

                //val = termoMatrix[32 * (23-y) + x];
                val = termoMatrix[32 * y + x];
                heatImage.at<float>(y,x) = val;
            }

#elif defined(MLX98641_DEV)
        for(int y=0; y<12; y++)
            for(int x=0; x<16; x++)
            {
                float val;

                //val = termoMatrix[16 * (12-y) + x];
                val = termoMatrix[16 * y + x];
                heatImage.at<float>(y,x) = val;
            }

        //MLX90641_GetImage(MLX90641_frame, &mlx90641, heatImage);

#endif
    }

    catch(Exception &e)
    {
        ObjectError("Building primitive image - " <<  e.msg);
    }

    /*if (!SkImageUtils::doRotation(heatImage, heatImage, ROTATE_90_COUNTERCLOCKWISE))
        return;

    SkImageUtils::doResize(heatImage, heatImage, camSize);*/
}
