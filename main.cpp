#include "robotthermocamcapture.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    SkCli *cli = skApp->appCli();

    cli->add("--i2c-device",            "-d", "/dev/i2c-1", "Setup the i2c bus");
    cli->add("--resolution",            "-r", "400x300",    "Setup the frame resolution");
    cli->add("--compression",           "-c", "80",         "Setup the jpeg-compression [1, 100]");
    cli->add("--fps",                   "-f", "4",         "Setup the frames per second value (allowed: 1,2,4,8,16,32,64");

#if defined(ENABLE_HTTP)
    SkFlowSat::addHttpCLI();
#endif

    skApp->init(5000, 150000, SK_TIMEDLOOP_SLEEPING);
    new RobotThermocamCapture;

    return skApp->exec();
}
