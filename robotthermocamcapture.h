#ifndef ROBOTTHERMOCAMCAPTURE_H
#define ROBOTTHERMOCAMCAPTURE_H

#include <Core/System/Network/FlowNetwork/skflowsat.h>
#include "Core/System/Network/FlowNetwork/skflowvideopublisher.h"
#include <Multimedia/Image/skmat.h>
#include <Core/System/skcli.h>

#define TA_SHIFT 8 //Default shift for MLX906X in open air

#if defined(MLX98640_DEV)
    #include <Core/System/Drivers/mlx/MLX90640_API.h>

#elif defined(MLX98641_DEV)
    #include <Core/System/Drivers/mlx/MLX90641_API.h>

#endif

enum SkThermoCapFps
{
    TCAP_FPS_NULL,
    TCAP_FPS_1,     //1
    TCAP_FPS_2,     //2
    TCAP_FPS_4,     //3
    TCAP_FPS_8,     //4
    TCAP_FPS_16,    //5
    TCAP_FPS_32,    //6
    TCAP_FPS_64     //7
};

class RobotThermocamCapture extends SkFlowSat
{
    bool enabled;
    int fps;
    int compression;
    Size resolution;

    SkMat f;
    SkFlowVideoPublisher *publisher;

    SkString i2cDev;

    Mat heatImage;
    Mat normal_mat;
    Mat u8_mat;
    Mat size_mat;

    SkFlowChanID matrixChan;

    Size camSize;
    ULong frameRawSize;
    int refreshRate;
    SkThermoCapFps refreshValue;
    float emissivity;
    int gradient;

#if defined(MLX98640_DEV)
    uint16_t MLX90640_frame[834];
    uint16_t eeMLX90640[832];
    float termoMatrix[768];
    paramsMLX90640 mlx90640;

#elif defined(MLX98641_DEV)
    uint16_t MLX90641_frame[242];
    uint16_t eeMLX90641[832];
    float termoMatrix[192];
    paramsMLX90641 mlx90641;

#endif
    public:
        Constructor(RobotThermocamCapture, SkFlowSat);

    private:
        bool onSetup()              override;
        void onInit()               override;
        void onQuit()               override;

        void onFastTick()           override;

        void grabData();
        void buildPrimitiveFrame();
};

#endif // ROBOTTHERMOCAMCAPTURE_H
